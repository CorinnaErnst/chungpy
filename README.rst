=======
ChungPy
=======

An implementation of Chung \& Lu [#chung]_'s linear time algorithm for solution of the Maximum Density Segment Problem with additional features, namely simultaneous identification of Minimum Density Segments and consideration of content constraints

.. [#chung] K.-M. Chung and H.-I. Lu. An optimal algorithm for the maximum-density segment problem. SIAM Journal on Computing, 34(2), 2005.


Requirements
------------

* Python >= 3.2
* Numpy >= 1.7

Installation
------------

Using easy_install, all dependencies will be installed automatically if missing::

   $ easy_install3 chungpy

When installing manually from setup.py, just execute::

   $ python3 setup.py install

Usage
-----

http://chungpy.readthedocs.org


Copyright (c) 2014 Corinna Ernst <corinna.ernst@uni-due.de> (see LICENSE)